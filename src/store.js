import { writable, get } from "svelte/store";
import { START, SHOW_ROLES, TIMER, RESULT } from "./views";
import dict from "./dict";
function shuffle(a) {
  for (let i = a.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [a[i], a[j]] = [a[j], a[i]];
  }
  return a;
}

export const view = writable(START);

export const playerCount = writable(4);
export const activePlayerIndex = writable(0);
export const pass = writable("");
export const time = writable(10);
export const players = (() => {
  const { subscribe, update } = writable([]);
  return {
    subscribe,
    start: () => {
      const agnetCount = Math.ceil(get(playerCount) * 0.3);
      update(() => {
        const shuffled = shuffle([
          ...new Array(agnetCount).fill("agent"),
          ...new Array(get(playerCount) - agnetCount).fill("citizen")
        ]);
        return shuffled;
      });
    }
  };
})();

export const startGame = () => {
  view.set(SHOW_ROLES);
  players.start();
  activePlayerIndex.set(0);
  pass.set(shuffle([...dict])[0]);
};

export const nextPlayer = () => {
  if (get(activePlayerIndex) === get(playerCount) - 1) {
    view.set(TIMER);
  } else {
    activePlayerIndex.update((i) => i + 1);
  }
};

export const endGame = () => {
  view.set(RESULT);
};

export const newGame = () => view.set(START);
