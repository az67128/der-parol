import Start from "./Start.svelte";
import ShowRoles from "./ShowRoles.svelte";
import Timer from "./Timer.svelte";
import Result from "./Result.svelte";

export const START = "START";
export const SHOW_ROLES = "SHOW_ROLES";
export const TIMER = "TIMER";
export const RESULT = "RESULT";
const views = {
  [START]: Start,
  [SHOW_ROLES]: ShowRoles,
  [TIMER]: Timer,
  [RESULT]: Result
};

export default views;
